/**
 * Класс для организации пользовательского интерфейса.
 */

public class UI {
    private static final String programVersion = "1.23";
    private static final String programName = "ReqRes.in user check";
    private static final String developYear = "2022";

    /**
     * Основной метод, принимающий параметр пользователя.
     *
     * @param args - аргументы, переданные при вызове программы. Учитывается первый из них {@link UI#parseUserIdFromArgs(String[])} ()}
     */
    public static void main(String[] args) {
        // текст приветствия
        showWelcomeScreen();

        // валидация аргументов программы
        if (!validateArg(args)) {
            System.out.println("Wrong or missing argument.");
            return;
        }

        // проверка доступности сервиса
        if (Library.checkWebServiceAvailability()) {
            System.out.println("Web service is online.");
        } else {
            System.out.println("Web service is not available at the moment. Please, try again later.");
            return;
        }

        // парсинг аргумента и отправка запроса
        int userId = parseUserIdFromArgs(args);
        String response = Library.sendRequest(userId);

        // Если ответ пустой, значит пользователя с таким идентификатором не существует
        if (response==null){
            System.out.println("User not found!");
            return;
        }

        // парсинг ответа
        String parsedResponse = Library.parseFirstAndLastNameFromJSON(response);

        // вывод результата
        System.out.println(parsedResponse);
    }

    /**
     * Выводит информацию:
     * - версию ПО;
     * - описание работы;
     * - справку по аргументам
     */
    private static void showWelcomeScreen() {
        String welcomeTitle = String.format("%s, ver. %s, %s", programName, programVersion, developYear);
        String programDescription = "Программа предназначена для отправки запроса на веб-сервис reqres.in и вывода результата.";
        String programArgumentsInfo = "Укажите идентификатор пользователя после названия программы. " +
                "Пример: java CVTReqReslatipov 22";

        System.out.println(welcomeTitle);
        System.out.println(programDescription);
        System.out.println(programArgumentsInfo);
    }

    /**
     * Валидация аргументов: наличие, конвертируемость.
     *
     * @param args аргументы, переданные в программу
     * @return false, если аргументов нет или извлечь из них число невозможно.
     */
    private static boolean validateArg(String[] args) {
        // переданы ли аргументы в программу
        if (args.length == 0) {
            return false;
        }
        // извлекаем первый аргумент и пробуем парсить
        String firstArgument = args[0];
        try {
            Integer.parseInt(firstArgument);
        } catch (NumberFormatException ex) {
            return false;
        }

        return true;
    }

    /**
     * Извлекает целочисленный идентификатор из переданных в программу аргументов.
     * Учитывается только первый аргумент, остальное игнорируется.
     *
     * @param args - аргументы, переданные при запуске прораммы {@link UI#main(String[])}
     * @return целочисленный идентификатор пользователя
     */
    private static int parseUserIdFromArgs(String[] args) {
        return Integer.parseInt(args[0]);
    }
}
