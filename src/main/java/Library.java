/**
 * Библиотека для работы с сервисом https://reqres.in и его ответом
 */

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class Library {
    private static String reqResLink = "https://reqres.in/api/users/";

    /**
     * Создаёт объект HTTP подключения по адресу
     *
     * @param link - адрес подключения
     * @return об
     */
    private static HttpURLConnection getConnection(String link) {
        // адрес ресурса
        URL reqResUrl = null;
        try {
            reqResUrl = new URL(link);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        // открываем подключение к сервису
        HttpURLConnection connection = null;
        try {
            assert reqResUrl != null;
            connection = (HttpURLConnection) reqResUrl.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return connection;
    }

    /**
     * Проверка доступности сервиса https://reqres.in
     *
     * @return false, если не доступен
     */
    static boolean checkWebServiceAvailability() {

        HttpURLConnection connection = getConnection(reqResLink);

        // определяем HTTP-метод
        try {
            connection.setRequestMethod("HEAD");
        } catch (ProtocolException e) {
            e.printStackTrace();
        }

        // получаем код состояния HTTP
        int responseCode = 0;
        try {
            responseCode = connection.getResponseCode();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return responseCode == 200;
    }

    /**
     * Отправка запроса и получение ответа от сервиса https://reqres.in
     *
     * @param userId - идентификатор пользователя
     * @return ответ сервиса https://reqres.in
     */
    static String sendRequest(int userId) {
        // открываем подключение к сервису
        HttpURLConnection connection = getConnection(reqResLink + userId);

        // определяем HTTP-метод
        try {
            connection.setRequestMethod("GET");
        } catch (ProtocolException e) {
            e.printStackTrace();
        }

        // открываем входной поток из подключения
        InputStream inputStream = null;
        try {
            inputStream = connection.getInputStream();
        } catch (FileNotFoundException e) {
            // отсутствует текст в ответе - пользователь не найден
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        }

        // читаем строки из входного потока
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
        String inputLine = null;
        StringBuffer result = new StringBuffer();

        while (true) {
            try {
                if ((inputLine = in.readLine()) == null) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            result.append(inputLine);
        }

        // закрываем входной поток
        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result.toString();
    }

    /**
     * Парсит имя и фамилию пользователя из JSON, высланного сервисом
     * @param response - ответ сервиса
     * @return <имя фамилия>
     */
    static String parseFirstAndLastNameFromJSON(String response) {
        // Считываем json
        Object obj = null;
        try {
            obj = new JSONParser().parse(response);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        // Кастим obj в JSONObject
        JSONObject jo = (JSONObject) obj;

        // Достаём firstName and lastName
        JSONObject dataNode = (JSONObject) jo.get("data");
        String firstName = (String) dataNode.get("first_name");
        String lastName = (String) dataNode.get("last_name");

        return String.format("%s %s", firstName, lastName);
    }
}
